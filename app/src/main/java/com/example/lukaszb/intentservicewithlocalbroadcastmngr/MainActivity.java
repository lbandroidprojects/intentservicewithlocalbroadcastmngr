package com.example.lukaszb.intentservicewithlocalbroadcastmngr;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button bLoad, bCancel;
    private ProgressBar pb;
    private DownloadStatusReceiver dsr;
    private static final int EXTERNAL_STORAGE_CODE = 112;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bLoad = findViewById(R.id.bDownload);
        bLoad.setOnClickListener(this);
        bCancel = findViewById(R.id.bCancel);
        bCancel.setOnClickListener(this);
        pb = findViewById(R.id.progressBar);

        dsr=new DownloadStatusReceiver();
        IntentFilter loadFinishedFilter=new IntentFilter(DownloadService.ACTION_FINISHED);
        LocalBroadcastManager.getInstance(this).registerReceiver(dsr,loadFinishedFilter);
        IntentFilter progressStatusFilter=new IntentFilter(DownloadService.ACTION_PROGRESS);
        LocalBroadcastManager.getInstance(this).registerReceiver(dsr,progressStatusFilter);
        IntentFilter cancelledStatusFilter=new IntentFilter(DownloadService.ACTION_CANCELLED);
        LocalBroadcastManager.getInstance(this).registerReceiver(dsr,cancelledStatusFilter);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.bDownload:
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                if (netwotkActive()) {
                    DownloadService.startActionLoad(this, "http://unesdoc.unesco.org/images/0023/002319/231920E.pdf");
                }
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_CODE);
            }
            break;
            case R.id.bCancel:
                Intent cancelIntent=new Intent(DownloadService.ACTION_CANCEL);
                LocalBroadcastManager.getInstance(this).sendBroadcast(cancelIntent);
                break;
        }
    }

    private boolean netwotkActive() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isAvailable() && ni.isConnected();
    }

    private class DownloadStatusReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case DownloadService.ACTION_FINISHED:
                    Toast.makeText(getApplicationContext(), "Ladowanie pliku zakończone",Toast.LENGTH_SHORT).show();
                    break;
                case DownloadService.ACTION_PROGRESS:
                    pb.setProgress(intent.getIntExtra("PROGRESS",0));
                    break;
                case DownloadService.ACTION_CANCELLED:
                    Toast.makeText(getApplicationContext(), "Ladowanie pliku zostało przerwane",Toast.LENGTH_SHORT).show();
            }
        }
    }
}
