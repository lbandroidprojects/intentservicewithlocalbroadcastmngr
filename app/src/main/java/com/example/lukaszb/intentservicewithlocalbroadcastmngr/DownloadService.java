package com.example.lukaszb.intentservicewithlocalbroadcastmngr;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class DownloadService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String ACTION_LOAD = "com.example.lukaszb.intentservicewithlocalbroadcastmngr.action.LOAD";
    public static final String ACTION_FINISHED = "com.example.lukaszb.intentservicewithlocalbroadcastmngr.action.FINISHED";
    public static final String ACTION_PROGRESS = "com.example.lukaszb.intentservicewithlocalbroadcastmngr.action.PROGRESS";
    public static final String ACTION_CANCEL = "com.example.lukaszb.intentservicewithlocalbroadcastmngr.action.CANCEL";
    public static final String ACTION_CANCELLED = "com.example.lukaszb.intentservicewithlocalbroadcastmngr.action.CANCELLED";
    private boolean cancelledFlag=false;
    private CancelActionReceiver car;

    // TODO: Rename parameters
    private static final String PARAM_URL = "com.example.lukaszb.intentservicewithlocalbroadcastmngr.extra.PARAM_URL";

    public DownloadService() {
        super("DownloadService");
        car=new CancelActionReceiver();
        IntentFilter actionCancelFilter=new IntentFilter(ACTION_CANCEL);
        LocalBroadcastManager.getInstance(this).registerReceiver(car,actionCancelFilter);
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionLoad(Context context, String param) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_LOAD);
        intent.putExtra(PARAM_URL, param);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_LOAD.equals(action)) {
                final String param = intent.getStringExtra(PARAM_URL);
                handleActionLoad(param);
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionLoad(String param) {
        int totalSize=0,currentSize=0;
        URL url = null;
        try {
            url = new URL(param);
            try (InputStream is = url.openStream();
                 FileOutputStream fos = new FileOutputStream(localDirectory()
                         + "/" + extractFileName(param))) {

                totalSize = url.openConnection().getContentLength();
                int bufsize = 2048;
                byte[] buffer = new byte[bufsize];
                int count;
                while ((count = is.read(buffer, 0, bufsize)) > -1) {
                    if (!cancelledFlag) {
                        fos.write(buffer, 0, count);
                        currentSize += count;
                        int procent = (int) ((double) (currentSize * 100) / ((double) totalSize));
                        Intent progressIntent = new Intent(ACTION_PROGRESS).putExtra("PROGRESS", procent);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(progressIntent);
                    }
                    else{
                        break;
                    }
                }
                if(!cancelledFlag) {
                    Intent finishedIntent = new Intent(ACTION_FINISHED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(finishedIntent);
                }
                else
                {
                    Intent cancelledIntent=new Intent(ACTION_CANCELLED);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(cancelledIntent);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    private String localDirectory() {
        return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();
    }

    private String extractFileName(String path) {
        return path.substring(path.lastIndexOf("/") + 1);
    }

    private class CancelActionReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()){
                case ACTION_CANCEL:
                   cancelledFlag=true;
            }
        }
    }
}